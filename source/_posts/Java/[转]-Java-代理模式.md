---
title: (转)Java 代理模式
date: '2018-08-25 22:43'
tags:
  - Java
  - 设计模式
categories: Java
abbrlink: 4dd31fbe
---

原文：https://www.cnblogs.com/cenyu/p/6289209.html
> 代理(Proxy)是一种设计模式，通过代理对象访问目标对象.这样做的好处是:可以在目标对象实现的基础上，扩展目标对象的功能。

<!-- more -->

举个例子来说明：假如说我现在想买一辆二手车，最便捷的方法一定是我去找中介，他们来给我做琐碎的事情，我只是负责选择自己喜欢的车，然后付钱就可以了。
![](http://ww1.sinaimg.cn/large/005HdV9dly1g1udu5ytxcj30fr0883yu.jpg)

**代理对象**是对**目标对象**的扩展,并会调用**目标对象**  


#### 静态代理
```
public interface IUserDao {

	void save();
	
}
```
```
public class UserDao implements IUserDao {

	@Override
	public void save() {
		System.out.println("----已经保存数据!----");
	}

}
```
```
public class UserDaoProxy implements IUserDao {
    //目标对象
    private IUserDao target;
    
    public UserDaoProxy(IUserDao target){
        this.target=target;
    }

    @Override
    public void save() {
        System.out.println("开始事务...");
        target.save();//执行目标对象的方法
        System.out.println("提交事务...");
    }

}
```
```
	@Test
	public void staticProxy() {
		//目标对象
        UserDao target = new UserDao();
        //代理对象，建立代理关系
        UserDaoProxy proxy = new UserDaoProxy(target);
        proxy.save();//执行的是代理的方法
	}
```
静态代理的缺点：接口新增方法时，类过多时，需要手动维护，过于繁琐，但是通过动态代理机制可以解决这一问题。
#### 动态代理
spring 中 AOP 通过 动态代理 在运行时为我们的代码增强，例如我们在开发的时候只需要做业务逻辑，AOP 通过动态代理可以为我们做事务，日志管理，权限校验等等。
##### jdk 动态代理
我们不需要再去手动创建代理类**（但是要求被代理类必须实现一个接口）**，只需要做一个动态代理工厂即可，jdk通过反射为我们在内存中动态创建代理对象，以及在方法执行的前后添加通知。
```java
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 创建动态代理对象
 * 
 */
public class JdkProxyFactory{

    //维护一个目标对象
    private Object target;
    public JdkProxyFactory(Object target){
        this.target=target;
    }

   //给目标对象生成代理对象
    public Object getProxyInstance(){
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("开始事务2");
                        //执行目标对象方法
                        Object returnValue = method.invoke(target, args);
                        System.out.println("提交事务2");
                        return returnValue;
                    }
                }
        );
    }

}
```
```
	@Test
	public void jdkProxy() {
		 // 目标对象
        IUserDao target = new UserDao();
        // 原始类型
        System.out.println("原始类型" + target.getClass());

        // 给目标对象，创建代理对象
        IUserDao proxy = (IUserDao) new JdkProxyFactory(target).getProxyInstance();
        // class $Proxy0   内存中动态生成的代理对象
        System.out.println("动态代理后对象类型:" + proxy.getClass());

        // 代理对象 执行方法 
        proxy.save();
	}
```
执行测试方法
```
原始类型class com.example.demo.original_code.UserDao
动态代理后对象类型:class com.sun.proxy.$Proxy4
开始事务2
----已经保存数据!----
提交事务2
```
##### cglib 动态代理
静态代理 和 jdk代理 要求目标对象必须实现接口，而 cglib 动态代理无需实现接口。
cglib 会动态为目标对象 创建一个子类对象。
**使用须知：**
1.被代理的类不能为 final
2.被代理类的方法 不能为 final/static，否则无法被拦截
```java
import java.lang.reflect.Method;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

/**
 * Cglib子类代理工厂
 * 对UserDao在内存中动态构建一个子类对象
 */
public class CglibProxyFactory implements MethodInterceptor {
    //维护目标对象
    private Object target;

    public CglibProxyFactory(Object target) {
        this.target = target;
    }

    //给目标对象创建一个代理对象
    public Object getProxyInstance(){
        //1.工具类
        Enhancer en = new Enhancer();
        //2.设置父类
        en.setSuperclass(target.getClass());
        //3.设置回调函数
        en.setCallback(this);
        //4.创建子类(代理对象)
        return en.create();

    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        System.out.println("开始事务...");

        //执行目标对象的方法
        Object returnValue = method.invoke(target, args);
        
        System.out.println("提交事务...");

        return returnValue;
    }
}
```
```
	@Test
	public void cglibProxy() {
		//目标对象
		UserDao target = new UserDao();

        //代理对象
		UserDao proxy = (UserDao) new CglibProxyFactory(target).getProxyInstance();

        //执行代理对象的方法
        proxy.save();
	}
```
