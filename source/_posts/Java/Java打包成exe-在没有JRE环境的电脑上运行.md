---
title: Java打包成exe 在没有JRE环境的电脑上运行
date: '2019-02-25 20:27'
tags:
  - Java
  - swing
categories: Java
abbrlink: 33eaa3e0
---

> 公司业务需求原因，需要给用户提供一个桌面应用程序。由于时间关系，没有考虑.net，使用了江湖上失传已久的Java Swing

<!-- more -->

### 准备工作
- 将你的Java项目打包为 **可执行Jar**

### 使用 exe4j 生成.exe
1. **新建一个文件夹，将你的 JRE（与JDK同级别的那个JRE），可执行jar 都复制过来**，下图除红圈以外的Jar 为项目的依赖Jar 包

![](http://ww1.sinaimg.cn/large/005HdV9dly1g1udure9kij30i40eatam.jpg)


2. 打开  exe4j ，Next

![](http://ww1.sinaimg.cn/large/005HdV9dly1g1uduzsyf8j30nm0i2q4y.jpg)

3. 选择 JAR IN EXE

![](http://ww1.sinaimg.cn/large/005HdV9dly1g1udv95h3qj30nm0i2jsj.jpg)

4. 选择刚刚我们新建的文件夹 （包含JRE，和项目可执行JAR） 

![](http://ww1.sinaimg.cn/large/005HdV9dly1g1udvfb99vj30nm0i2aaz.jpg)

5. 这里是设置文件名，图标，是否仅允许一个应用实例等等，**需要注意的是，64位的 jdk是需要设置一下的**，如图

![](http://ww1.sinaimg.cn/large/005HdV9dly1g1udvpclhzj30nm0i2dh6.jpg)

6. 这里是将项目的，可执行JAR，还有依赖JAR 全部添加进来，**注意一定要使用相对路径**，Main class from 选择一个项目的启动类。

![](http://ww1.sinaimg.cn/large/005HdV9dly1g1udvwjn2wj30nm0i2mz6.jpg)

7.  设置 Search sequence

![](http://ww1.sinaimg.cn/large/005HdV9dly1g1udwlvdjrj30nm0i2q44.jpg)

清除默认配置，设置JRE，**使用相对路径**

![](http://ww1.sinaimg.cn/large/005HdV9dly1g1udx16cdej30nm0i2jsg.jpg)

8. 后几步默认配置，下一步即可。

### 备注
- 需要注意的是，JAR和JRE 一定要使用相对路径（绝对路径无法保证你的路径和用户电脑一致）
- exe4j，没有激活之前生成的exe，启动之前会跳个对话框，激活即可
- 生成的exe 不依赖之前的JAR，只依赖JRE

> 参考：https://www.cnblogs.com/lsy-blogs/p/7668425.html
