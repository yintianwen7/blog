---
title: MySQL-并发下生成不重复流水号
date: '2018-01-14 13:55'
tags:
  - MySQL
  - 并发
categories: MySQL
abbrlink: ef3d0933
---

> 更新于 2018-12-23 22:21:44
前言：一年前的写的，当时的做法并不能在并发下保证流水号的唯一性，因为当时并没有写多线程测试过...

<!--more-->

##### 思路
| sCode | sName | sQz | sValue |
| ------ | ------ | ------ | ------ |
| order	| 订单 | DD | 18120100 |

1. 首先每个业务的流水号对应表中的一条数据
2. 每个要获取流水号的线程调用 一个用来生成流水号的 **存储过程**
2. 根据sCode 找到 sValue。
```
if (sValue == null) 
    初始化值 
else 
    sValue ++
```
##### 敲黑板！划重点！
- 分析：上述的思路看似没什么问题，但是如果**两个线程并发**的执行会出现**两个事务读取到相同的数据**，同时都执行初始化或者自增。
- 方案：使用 **MySQL 写锁（select...for update，也叫X锁，排它锁）** 来解决这一问题
- 例如：事务A和事务B同时进行，事务A 拿到 当前这条数据的**写锁**，此时如果事务B 想访问这条数据需要等待事务A结束。并发时让两个事务对同一条数据的操作变成了串行化。



```
CREATE TABLE `sys_sno` (
  `sCode` varchar(50) DEFAULT NULL COMMENT '编码',
  `sName` varchar(100) DEFAULT NULL COMMENT '名称',
  `sQz` varchar(50) DEFAULT NULL COMMENT '前缀',
  `sValue` varchar(80) DEFAULT NULL COMMENT '值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

```

```
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetSerialNo`(IN tsCode VARCHAR(50),OUT result VARCHAR(200) )
BEGIN   
   
   DECLARE  tsValue  VARCHAR(50);  
   DECLARE  tdToday  VARCHAR(20);       
   DECLARE  nowdate  VARCHAR(20);        
   DECLARE  tsQZ     VARCHAR(50);  
   DECLARE t_error INTEGER DEFAULT 0;    
   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET t_error=1;    
   START TRANSACTION;    
     /* UPDATE sys_sno  SET sValue=sValue WHERE sCode=tsCode;  */
      SELECT sValue INTO tsValue  FROM sys_sno  WHERE sCode=tsCode for UPDATE;  
      SELECT sQz INTO tsQZ FROM sys_sno WHERE sCode=tsCode ;  
    -- 因子表中没有记录，插入初始值     
      IF tsValue IS NULL  THEN   
          SELECT CONCAT(DATE_FORMAT(NOW(),'%y%m'),'0001') INTO tsValue;  
          UPDATE sys_sno SET sValue=tsValue WHERE sCode=tsCode ;  
          SELECT CONCAT(tsQZ,tsValue) INTO result;  
      ELSE                  
          SELECT  SUBSTRING(tsValue,1,4) INTO tdToday;  
          SELECT  CONVERT(DATE_FORMAT(NOW(),'%y%m'),SIGNED) INTO nowdate;
          -- 判断年月是否需要更新
          IF tdToday = nowdate THEN  
             SET  tsValue=CONVERT(tsValue,SIGNED) + 1;  
          ELSE  
             SELECT CONCAT(DATE_FORMAT(NOW(),'%y%m') ,'0001') INTO tsValue ;  
          END IF;  
          UPDATE sys_sno SET sValue =tsValue WHERE sCode=tsCode;  
          SELECT CONCAT(tsQZ,tsValue) INTO result;  
     END IF;  
        
     IF t_error =1 THEN    
       ROLLBACK;    
       SET result = 'Error';  
     ELSE    
        COMMIT;    
     END IF;   
     SELECT  result ;     
END;
```
##### 测试代码 
**完整测试代码：https://gitee.com/yintianwen7/taven-springboot-learning/tree/master/uni-number**
```
import com.gitee.taven.uninumber.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class OrderService {

    private static OrderMapper orderMapper;

    @Autowired
    public void setOrderMapper(OrderMapper orderMapper) {
        this.orderMapper = orderMapper;
    }

    private static final int TOTAL_THREADS = 100;

    public void multiThread() {
        // 创建固定长度线程池
        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(50);
        for (int i = 0; i < TOTAL_THREADS; i++) {
            Thread thread = new OrderThread();
            fixedThreadPool.execute(thread);
        }

    }

    public static class OrderThread extends Thread {

        @Override
        public void run() {
            try {
                Map<String, String> parameterMap = initParameterMap();
                orderMapper.createOrderNum(parameterMap);
                String number = parameterMap.get("result");
                System.out.println(Thread.currentThread().getName() + " : " +number);

            } catch (Exception e) {
                System.out.println(e);

            }
        }

        public Map<String, String> initParameterMap() {
            Map<String, String> parameterMap = new HashMap<>();
            parameterMap.put("tsCode", "order");
            parameterMap.put("result", "-1");
            return parameterMap;
        }

    }

}
```
```
// mapper 接口
public interface OrderMapper {

    int createOrderNum(Map<String, String> parameterMap);

}

// xml
    <update id="createOrderNum" parameterMap="initMap" statementType="CALLABLE">
        CALL GetSerialNo(?,?)
    </update>

    <parameterMap type="java.util.Map" id="initMap">
        <parameter property="tsCode" mode="IN" jdbcType="VARCHAR"/>
        <parameter property="result" mode="OUT" jdbcType="VARCHAR"/>
    </parameterMap>
```
##### 备注
- 执行了一个百个线程之后 可以看一下数据，自增到了100 说明成功了
- 删掉 存储过程中的 for update，再执行 可以看出锁的作用
