---
title: Spring 源码解析 @RequestBody @ResponseBody 的来龙去脉
date: '2019-05-16 10:47'
tags:
  - SpringBoot
  - 源码
keyword:
  - SpringBoot
  - 源码
  - RequestBody
  - ResponseBody
categories: SpringBoot
abbrlink: b75109d6
---

<meta name="referrer" content="no-referrer" />

> @RequestBody 和 @ResponseBody 是实际开发中很常用的两个注解，通常用来解析和响应JSON，用起来十分的方便，这两个注解的背后是如何实现的？

<!-- more -->

#### 源码版本
SpringBoot 2.1.3.RELEASE

#### RequestResponseBodyMethodProcessor

> Resolves method arguments annotated with @RequestBody and handles return values from methods annotated with @ResponseBody by reading and writing to the body of the request or response with an HttpMessageConverter.
An @RequestBody method argument is also validated if it is annotated with @javax.validation.Valid. In case of validation failure, MethodArgumentNotValidException is raised and results in an HTTP 400 response status code if DefaultHandlerExceptionResolver is configured.

简单来说，这个类用来解析`@RequestBody`的参数和处理 `@ResponseBody`返回值，通过 HttpMessageConverter 这个接口来实现。

如果`@RequestBody`标记的参数包含`@Valid`，还会对这个参数进行校验。

#### 继承关系

![image.png](https://upload-images.jianshu.io/upload_images/9949918-facd51ad496af1be.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

HandlerMethodArgumentResolver 和 HandlerMethodReturnValueHandler 分别是Spring的参数处理器和返回值处理器

- HandlerMethodArgumentResolver 
```java
public interface HandlerMethodArgumentResolver {

	boolean supportsParameter(MethodParameter parameter);

	Object resolveArgument(MethodParameter parameter, @Nullable ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, @Nullable WebDataBinderFactory binderFactory) throws Exception;

}
```
Spring的参数解析器接口，supportsParameter() 方法用于判断解析器是否支持当前Controller方法的参数，resolveArgument() 则是将Request解析为Controller方法对应的参数Bean

- HandlerMethodReturnValueHandler 
```java
public interface HandlerMethodReturnValueHandler {

	boolean supportsReturnType(MethodParameter returnType);

	void handleReturnValue(@Nullable Object returnValue, MethodParameter returnType,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception;

}
```
同理这个接口将Controller方法返回的对象，封装为Response

我们在实际开发时，也可以实现这两个接口自定义自己的参数解析和响应处理，RequestResponseBodyMethodProcessor 实现了这两个接口，既做了参数解析器也做了响应处理器。

#### RequestResponseBodyMethodProcessor 源码分析

我们来看一下 RequestResponseBodyMethodProcessor 是如何工作的，以解析参数为例

- resolveArgument

<!--
![carbon (5).png](https://upload-images.jianshu.io/upload_images/9949918-cb1a37e5ee671aa1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240) -->

```java
@Override
public boolean supportsParameter(MethodParameter parameter) {
    // 支持标记@RequestBody的参数
    return parameter.hasParameterAnnotation(RequestBody.class);
}

@Override
public Object resolveArgument(MethodParameter parameter, @Nullable ModelAndViewContainer mavContainer,
        NativeWebRequest webRequest, @Nullable WebDataBinderFactory binderFactory) throws Exception {

    parameter = parameter.nestedIfOptional();
    // 通过HttpMessageConverters 将请求体, 封装为@RequestBody所标记的XXBean
    Object arg = readWithMessageConverters(webRequest, parameter, parameter.getNestedGenericParameterType());
    String name = Conventions.getVariableNameForParameter(parameter);

    if (binderFactory != null) {
        WebDataBinder binder = binderFactory.createBinder(webRequest, arg, name);
        if (arg != null) {
            // 如果存在@Valid 对参数进行校验
            validateIfApplicable(binder, parameter);
            if (binder.getBindingResult().hasErrors() && isBindExceptionRequired(binder, parameter)) {
                throw new MethodArgumentNotValidException(parameter, binder.getBindingResult());
            }
        }
        if (mavContainer != null) {
            mavContainer.addAttribute(BindingResult.MODEL_KEY_PREFIX + name, binder.getBindingResult());
        }
    }

    return adaptArgumentIfNecessary(arg, parameter);
}

@Override
protected <T> Object readWithMessageConverters(NativeWebRequest webRequest, MethodParameter parameter,
        Type paramType) throws IOException, HttpMediaTypeNotSupportedException, HttpMessageNotReadableException {

    HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
    Assert.state(servletRequest != null, "No HttpServletRequest");
    ServletServerHttpRequest inputMessage = new ServletServerHttpRequest(servletRequest);

    Object arg = readWithMessageConverters(inputMessage, parameter, paramType);
    if (arg == null && checkRequired(parameter)) {
        throw new HttpMessageNotReadableException("Required request body is missing: " +
                parameter.getExecutable().toGenericString(), inputMessage);
    }
    return arg;
}
```


作为参数解析器，RequestResponseBodyMethodProcessor 支持所有标记@RequestBody的参数。在resolveArgument()方法中，通过调用readWithMessageConverters() 将 Request 转为对应 arg。我们来看一下 readWithMessageConverters() 到底做了什么

- readWithMessageConverters

```java
protected <T> Object readWithMessageConverters(HttpInputMessage inputMessage, MethodParameter parameter,
        Type targetType) throws IOException, HttpMediaTypeNotSupportedException, HttpMessageNotReadableException {

    // 当前请求的contentType
    MediaType contentType;
    boolean noContentType = false;
    try {
        contentType = inputMessage.getHeaders().getContentType();
    }
    catch (InvalidMediaTypeException ex) {
        throw new HttpMediaTypeNotSupportedException(ex.getMessage());
    }
    if (contentType == null) {
        noContentType = true;
        contentType = MediaType.APPLICATION_OCTET_STREAM;
    }

    // Controller参数的Class
    Class<?> contextClass = parameter.getContainingClass();
    Class<T> targetClass = (targetType instanceof Class ? (Class<T>) targetType : null);
    if (targetClass == null) {
        ResolvableType resolvableType = ResolvableType.forMethodParameter(parameter);
        targetClass = (Class<T>) resolvableType.resolve();
    }
    
    // 当前请求方式
    HttpMethod httpMethod = (inputMessage instanceof HttpRequest ? ((HttpRequest) inputMessage).getMethod() : null);
    Object body = NO_VALUE;

    EmptyBodyCheckingHttpInputMessage message;
    try {
        message = new EmptyBodyCheckingHttpInputMessage(inputMessage);
        
        // 遍历所有的HttpMessageConverter，
        for (HttpMessageConverter<?> converter : this.messageConverters) {
            Class<HttpMessageConverter<?>> converterType = (Class<HttpMessageConverter<?>>) converter.getClass();
            GenericHttpMessageConverter<?> genericConverter =
                    (converter instanceof GenericHttpMessageConverter ? (GenericHttpMessageConverter<?>) converter : null);
                    
            // 如果当前的HttpMessageConverter可以解析对应的 class和contentType
            if (genericConverter != null ? genericConverter.canRead(targetType, contextClass, contentType) :
                    (targetClass != null && converter.canRead(targetClass, contentType))) {
                if (message.hasBody()) {
                    HttpInputMessage msgToUse =
                            getAdvice().beforeBodyRead(message, parameter, targetType, converterType);
                            
                    // 将Http报文转换为对应的class
                    body = (genericConverter != null ? genericConverter.read(targetType, contextClass, msgToUse) :
                            ((HttpMessageConverter<T>) converter).read(targetClass, msgToUse));
                            
                    body = getAdvice().afterBodyRead(body, msgToUse, parameter, targetType, converterType);
                }
                else {
                    body = getAdvice().handleEmptyBody(null, message, parameter, targetType, converterType);
                }
                break;
            }
        }
    }
    catch (IOException ex) {
        throw new HttpMessageNotReadableException("I/O error while reading input message", ex, inputMessage);
    }

    if (body == NO_VALUE) {
        if (httpMethod == null || !SUPPORTED_METHODS.contains(httpMethod) ||
                (noContentType && !message.hasBody())) {
            return null;
        }
        throw new HttpMediaTypeNotSupportedException(contentType, this.allSupportedMediaTypes);
    }

    MediaType selectedContentType = contentType;
    Object theBody = body;
    LogFormatUtils.traceDebug(logger, traceOn -> {
        String formatted = LogFormatUtils.formatValue(theBody, !traceOn);
        return "Read \"" + selectedContentType + "\" to [" + formatted + "]";
    });

    return body;
}
```
上述代码核心逻辑就是遍历当前解析中配置的所有 HttpMessageConverter，如果某个Converter可以解析当前的 contentType，就把转换工作交给他去进行。

之前做过将默认解析替换为fastjson，当时就是添加一个FastJson实现的HttpMessageConverter，但是那时候并不理解这么做是为了什么，现在才恍然大悟...

- handleReturnValue

RequestResponseBodyMethodProcessor 的Response处理逻辑和解析逻辑类似，找到一个支持的HttpMessageConverter，把响应工作交给他，感兴趣的童鞋可以自己找下源码。

#### RequestResponseBodyMethodProcessor 是怎么被调用的
上面讲了 RequestResponseBodyMethodProcessor 做了参数解析和响应处理的工作，那么他在Spring框架中是怎么被调用的，我们来看一下

![image.png](https://upload-images.jianshu.io/upload_images/9949918-f7b539c1eb6a2939.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

如图，RequestMappingHandlerAdapter 的resolvers（Request解析器）、handlers（Response处理器）还有 ExceptionHandlerExceptionResolver 的handlers 调用了 RequestResponseBodyMethodProcessor 

##### RequestMappingHandlerAdapter 
我们只分析一下 RequestMappingHandlerAdapter ，该类对所有标记 @RequestMapping的注解进行解析和响应

在WebMvcConfigurationSupport中，配置了该Bean，将其加入到Spring容器中，我们自定义的参数解析、响应解析、和HttpMessageConvert 通过上图的方法set到 RequestMappingHandlerAdapter 中。

```java
@Bean
public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
    RequestMappingHandlerAdapter adapter = createRequestMappingHandlerAdapter();
    adapter.setContentNegotiationManager(mvcContentNegotiationManager());
    // 获取所有HttpMessageConverter，包括我们自定义的配置
    adapter.setMessageConverters(getMessageConverters());
    adapter.setWebBindingInitializer(getConfigurableWebBindingInitializer());
    // 自定义的参数解析器
    adapter.setCustomArgumentResolvers(getArgumentResolvers());
    // 自定义的响应处理器
    adapter.setCustomReturnValueHandlers(getReturnValueHandlers());

    if (jackson2Present) {
        adapter.setRequestBodyAdvice(Collections.singletonList(new JsonViewRequestBodyAdvice()));
        adapter.setResponseBodyAdvice(Collections.singletonList(new JsonViewResponseBodyAdvice()));
    }

    AsyncSupportConfigurer configurer = new AsyncSupportConfigurer();
    configureAsyncSupport(configurer);
    if (configurer.getTaskExecutor() != null) {
        adapter.setTaskExecutor(configurer.getTaskExecutor());
    }
    if (configurer.getTimeout() != null) {
        adapter.setAsyncRequestTimeout(configurer.getTimeout());
    }
    adapter.setCallableInterceptors(configurer.getCallableInterceptors());
    adapter.setDeferredResultInterceptors(configurer.getDeferredResultInterceptors());

    return adapter;
}
```


继续说 RequestMappingHandlerAdapter ，getDefaultArgumentResolvers() 封装了SpringBoot中的默认参数解析器，其中就有我们的本节所讲的 **RequestResponseBodyMethodProcessor** ，在afterPropertiesSet() 方法中调用了该方法

```java
@Override
public void afterPropertiesSet() {
    // Do this first, it may add ResponseBody advice beans
    initControllerAdviceCache();

    if (this.argumentResolvers == null) {
        List<HandlerMethodArgumentResolver> resolvers = getDefaultArgumentResolvers();
        this.argumentResolvers = new HandlerMethodArgumentResolverComposite().addResolvers(resolvers);
    }
    if (this.initBinderArgumentResolvers == null) {
        List<HandlerMethodArgumentResolver> resolvers = getDefaultInitBinderArgumentResolvers();
        this.initBinderArgumentResolvers = new HandlerMethodArgumentResolverComposite().addResolvers(resolvers);
    }
    if (this.returnValueHandlers == null) {
        List<HandlerMethodReturnValueHandler> handlers = getDefaultReturnValueHandlers();
        this.returnValueHandlers = new HandlerMethodReturnValueHandlerComposite().addHandlers(handlers);
    }
}

private List<HandlerMethodArgumentResolver> getDefaultArgumentResolvers() {
    List<HandlerMethodArgumentResolver> resolvers = new ArrayList<>();

    // Annotation-based argument resolution
    resolvers.add(new RequestParamMethodArgumentResolver(getBeanFactory(), false));
    resolvers.add(new RequestParamMapMethodArgumentResolver());
    resolvers.add(new PathVariableMethodArgumentResolver());
    resolvers.add(new PathVariableMapMethodArgumentResolver());
    resolvers.add(new MatrixVariableMethodArgumentResolver());
    resolvers.add(new MatrixVariableMapMethodArgumentResolver());
    resolvers.add(new ServletModelAttributeMethodProcessor(false));
    
    // 添加RequestResponseBodyMethodProcessor
    resolvers.add(new RequestResponseBodyMethodProcessor(getMessageConverters(), this.requestResponseBodyAdvice));
    
    // 省略，详见源码...

    return resolvers;
}

private List<HandlerMethodReturnValueHandler> getDefaultReturnValueHandlers() {
    List<HandlerMethodReturnValueHandler> handlers = new ArrayList<>();

    // Single-purpose return value types
    handlers.add(new ModelAndViewMethodReturnValueHandler());
    handlers.add(new ModelMethodProcessor());
    handlers.add(new ViewMethodReturnValueHandler());
    handlers.add(new ResponseBodyEmitterReturnValueHandler(getMessageConverters(),
            this.reactiveAdapterRegistry, this.taskExecutor, this.contentNegotiationManager));
    handlers.add(new StreamingResponseBodyReturnValueHandler());
    handlers.add(new HttpEntityMethodProcessor(getMessageConverters(),
            this.contentNegotiationManager, this.requestResponseBodyAdvice));
    handlers.add(new HttpHeadersReturnValueHandler());
    handlers.add(new CallableMethodReturnValueHandler());
    handlers.add(new DeferredResultMethodReturnValueHandler());
    handlers.add(new AsyncTaskMethodReturnValueHandler(this.beanFactory));

    // Annotation-based return value types
    handlers.add(new ModelAttributeMethodProcessor(false));
    
    // 添加RequestResponseBodyMethodProcessor
    handlers.add(new RequestResponseBodyMethodProcessor(getMessageConverters(),
            this.contentNegotiationManager, this.requestResponseBodyAdvice));

    // 省略，详见源码...

    return handlers;
}
```


##### RequestResponseBodyMethodProcessor 何时被调用
上面铺垫了这么多，终于来了

RequestMappingHandlerAdapter 的 invokeHandlerMethod 中 构建了 invocableMethod 对象并将所有的解析器和处理器封装到该对象，通过invocableMethod.invokeAndHandle() 进行对请求的解析，对controller的调用，以及响应的处理

![image.png](https://upload-images.jianshu.io/upload_images/9949918-b7818b84c00ba90b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

invocableMethod.invokeAndHandle() 中是怎么样实现的
```java
public void invokeAndHandle(ServletWebRequest webRequest, ModelAndViewContainer mavContainer,
        Object... providedArgs) throws Exception {
        
    // 参数解析，并反射调用controller方法，获取方法返回值
    Object returnValue = invokeForRequest(webRequest, mavContainer, providedArgs);
    
    // 下面就是对Response的处理
    setResponseStatus(webRequest);

    if (returnValue == null) {
        if (isRequestNotModified(webRequest) || getResponseStatus() != null || mavContainer.isRequestHandled()) {
            mavContainer.setRequestHandled(true);
            return;
        }
    }
    else if (StringUtils.hasText(getResponseStatusReason())) {
        mavContainer.setRequestHandled(true);
        return;
    }

    mavContainer.setRequestHandled(false);
    Assert.state(this.returnValueHandlers != null, "No return value handlers");
    try {
        this.returnValueHandlers.handleReturnValue(
                returnValue, getReturnValueType(returnValue), mavContainer, webRequest);
    }
    catch (Exception ex) {
        if (logger.isTraceEnabled()) {
            logger.trace(formatErrorForReturnValue(returnValue), ex);
        }
        throw ex;
    }
}

public Object invokeForRequest(NativeWebRequest request, @Nullable ModelAndViewContainer mavContainer,
        Object... providedArgs) throws Exception {
        
    // 调用参数解析器获取调用controller 所需的参数
    Object[] args = getMethodArgumentValues(request, mavContainer, providedArgs);
    if (logger.isTraceEnabled()) {
        logger.trace("Arguments: " + Arrays.toString(args));
    }
    
    // 反射调用 controller
    return doInvoke(args);
}

protected Object[] getMethodArgumentValues(NativeWebRequest request, @Nullable ModelAndViewContainer mavContainer,
        Object... providedArgs) throws Exception {

    if (ObjectUtils.isEmpty(getMethodParameters())) {
        return EMPTY_ARGS;
    }
    MethodParameter[] parameters = getMethodParameters();
    Object[] args = new Object[parameters.length];
    
    // 遍历解析参数
    for (int i = 0; i < parameters.length; i++) {
        MethodParameter parameter = parameters[i];
        parameter.initParameterNameDiscovery(this.parameterNameDiscoverer);
        args[i] = findProvidedArgument(parameter, providedArgs);
        if (args[i] != null) {
            continue;
        }
        
        // 这里的 resolvers 是一个封装了所有参数解析器的包装类，遍历所有解析器，如果不能找到支持当前参数的，抛出异常
        // 如果找到当前参数对应的解析器，则缓存起来，在下面的 resolvers.resolveArgument 时，直接使用
        if (!this.resolvers.supportsParameter(parameter)) {
            throw new IllegalStateException(formatArgumentError(parameter, "No suitable resolver"));
        }
        try {
            // 调用参数解析器
            args[i] = this.resolvers.resolveArgument(parameter, mavContainer, request, this.dataBinderFactory);
        }
        catch (Exception ex) {
            // Leave stack trace for later, exception may actually be resolved and handled..
            if (logger.isDebugEnabled()) {
                String error = ex.getMessage();
                if (error != null && !error.contains(parameter.getExecutable().toGenericString())) {
                    logger.debug(formatArgumentError(parameter, error));
                }
            }
            throw ex;
        }
    }
    return args;
}
```
invokeAndHandle() 里做了三件事
1. 将请求中解析为Controller中指定的参数
2. 用解析好的参数反射调用 Controller 方法
3. 处理响应

#### 一次Http请求经历了什么

回过头来再看，这时候我们发一个请求，在 RequestMappingHandlerAdapter 的 invokeHandlerMethod()中 debug一下，看一下线程栈是什么样的

![](https://upload-images.jianshu.io/upload_images/9949918-3e937d8da788b344.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/2480)

简单画一张图来表示一下

![](https://upload-images.jianshu.io/upload_images/9949918-a26b15d4bd7d14f7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

#### END

> 欢迎各位给出意见和指正
