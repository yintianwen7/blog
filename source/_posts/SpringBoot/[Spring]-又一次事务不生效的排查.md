---
title: Spring-又一次事务不生效的排查
date: '2018-12-19 22:53'
tags:
  - SpringBoot
  - AOP
  - Shiro
categories: SpringBoot
abbrlink: 836297d7
---

> 上一篇关于事务的 [解决 spring service 调用当前类方法事务不生效](https://www.jianshu.com/p/cb649e6152b6)

<!--more-->

#### 正文

今天在工作中突然发现标记了 **`@Transactional`** 的方法，事务并没有生效。在检查了业务层是否 try catch异常，MySQL存储引擎之后。心态已近崩溃的边缘：)
这个时候突然发现了一个神奇的现象！
![](http://ww1.sinaimg.cn/large/005HdV9dgy1g1qoh6wygtj30gt01rq2r.jpg)
两个`@Service` 都标记了 **`@Transactional`**，userService 并没有生成代理对象，也就导致了事务不生效。
***
继续排查，最后锁定了凶手 --- **BeanPostProcessor** 先看一下官方描述
```
/**
 * Factory hook that allows for custom modification of new bean instances,
 * e.g. checking for marker interfaces or wrapping them with proxies.
 *
 * <p>ApplicationContexts can autodetect BeanPostProcessor beans in their
 * bean definitions and apply them to any beans subsequently created.
 * Plain bean factories allow for programmatic registration of post-processors,
 * applying to all beans created through this factory.
 *
 * <p>Typically, post-processors that populate beans via marker interfaces
 * or the like will implement {@link #postProcessBeforeInitialization},
 * while post-processors that wrap beans with proxies will normally
 * implement {@link #postProcessAfterInitialization}.
 *
```
简单的来说，BeanPostProcessor是Spring 给我们提供的一个扩展接口
```
public interface BeanPostProcessor {
    // bean实例化方法调用前被调用
    Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException;
    // bean实例化方法调用后被调用
    Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException;

}
```

#### 分析

- ApplicationContexts 检测到 BeanPostProcessor 之后会将他应用于随后创建的所有 bean，所以 BeanPostProcessor 会在其他Bean的之前加载，但是随之引发的问题的就是 BeanPostProcessor 实现类所引用的Bean 没有被代理，只是被托管到 IOC 容器中。
- 我的项目里引用了 Shiro，而 Shiro 的所有组件最终会被封装到 ShiroFilterFactoryBean （该类实现了 **BeanPostProcessor**）中，而 Shiro 的 Realm 中又依赖了我们的 Service，该 Service 预先加载，导致没有被代理

#### 解决方案

```
public class AuthRealm extends AuthorizingRealm {

    @Autowired
    @Lazy // 延迟加载
    private UserService userService;

}
```

#### 总结

这里给大家简单介绍引起事务不生效的几个原因
- try catch 捕获 Service 运行时异常，因为 Spring 默认在捕获到 RuntimeException 时回滚
- MySQL存储引擎 InnoDB 是支持事务的，而 MyISAM 不支持
- 由于各种原因没有使用或者 Spring 没有生成代理对象
