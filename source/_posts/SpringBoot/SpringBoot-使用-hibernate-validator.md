---
title: SpringBoot 使用 hibernate validator
date: '2018-11-26 21:13'
tags:
  - SpringBoot
categories: SpringBoot
abbrlink: 81ef5d67
---

> 本文将全面的介绍如何使用 validator 进行数据校验

<!--more-->

> 本文源码：[https://gitee.com/yintianwen7/taven-springboot-learning/tree/master/springboot-validate](https://gitee.com/yintianwen7/taven-springboot-learning/tree/master/springboot-validate)

***
#### 准备工作
我们只需要引入 `spring-boot-starter-web`包即可使用

#### 常用注解

| 注解 | 释义 |
| ------ | ------ |
| @Valid	| 被注释的元素是一个对象，需要检查此对象的所有字段值
| @Null	| 被注释的元素必须为 null
| @NotNull	| 被注释的元素必须不为 null
|@NotEmpty    | 被注释的字符串的必须非空, 即不为null, ""
|@NotBlank     |  被注释的字符串的必须非空, 即不为null, "", "   "
| @AssertTrue	| 被注释的元素必须为 true
| @AssertFalse	| 被注释的元素必须为 false
| @Min(value)	| 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
| @Max(value)	| 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
| @DecimalMin(value)	| 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
| @DecimalMax(value)	| 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
| @Size(max, min)	| 被注释的元素的大小必须在指定的范围内
| @Digits (integer, fraction)	| 被注释的元素必须是一个数字，其值必须在可接受的范围内
| @Past	| 被注释的元素必须是一个过去的日期
| @Future	| 被注释的元素必须是一个将来的日期
| @Pattern(value)	| 被注释的元素必须符合指定的正则表达式
| @Email	| 被注释的元素必须是电子邮箱地址
| @Length(min=, max=)	| 被注释的字符串的大小必须在指定的范围内
| @Range(min=, max=)	| 被注释的元素必须在合适的范围




#### 简单的实体校验
```
public class CardDTO {

    @NotBlank
    private String cardId;

    @Size(min = 10, max = 10)
    @NotNull
    private String cardNum; // 卡号

    @Past
    @NotNull
    private Date createDate;

    @Range(max = 3)
    private String cardType;

    // 省略get set
}
```
```
@RestController
public class UserController {

    @PostMapping("simple")
    public Object simple(@RequestBody @Valid CardDTO cardDTO) {
        return cardDTO;
    }

}
```
- 实体属性上添加校验注解
- controller 方法 参数前 使用`@Valid` 即可

#### 复杂的实体校验
##### 嵌套实体校验
```
public class UserDTO {

    @NotBlank
    private String userId;

    @NotBlank
    private String username;

    private String password;

    @Valid
    private List<CardDTO> cardList;

    //省略 get set
}
```
- controller 写法 同上，只是在 UserDTO cardList 属性上标记@Valid 注解 即可。

##### List<DTO> 校验

<!-- ![无效示例](http://ww1.sinaimg.cn/large/005HdV9dly1g1ucoodjy2j30j102dmx9.jpg)

如果我们想校验 一个实体List，如上图所示的这种写法是完全不起效的。-->

- 我们需要像 **嵌套校验** 时一样，对`List<CardDTO>` 做一层封装
```
public class ValidList<E> implements List<E> {

	@Valid
	private List<E> list = new ArrayList<>();

	public List<E> getList() {
		return list;
	}

	public void setList(List<E> list) {
		this.list = list;
	}

    // 省略了 实现方法
}
```
- 重写实现方法完全使用 this.list.xxx() 
Gitee: 
spring 会将数据封装到我们定义的 list 属性中，又将属性声明了 @Valid 使得 hibernate validator 可以为我们做校验！

##### 使用 @Validated 分组校验
```
public interface Insert {
}

public interface Update {
}
```
- 定义两个接口
```
public class GroupCardDTO {

    @NotBlank(groups = {Update.class})
    private String id;

    @NotBlank(groups = {Insert.class})
    private String cardNum;

    @NotNull(groups = {Insert.class, Update.class})
    private Integer cardType;

    //省略 get set
}
```
- 实体标记的注解中添加 group 属性
```
    @PostMapping("insert_card")
    public Object insert_card(@RequestBody @Validated(Insert.class) GroupCardDTO card){
        return card;
    }
```
- 使用 @Validated(xxx.class) 标记参数，完成分组校验！

#### 自定义注解校验
当 validator 提供的注解无法满足我们的业务需求，可以通过自定义的方式来实现校验。

##### 需求：校验某字符串必须为大写或者小写
```
public enum CaseMode {
    UPPER,
    LOWER
}
```
- 定义一个枚举类

```java
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CheckCaseValidator.class)
@Documented
public @interface CheckCase {

    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    CaseMode value() default CaseMode.LOWER;

}
```
- 定义注解
- `@Constraint` 指定我们的校验逻辑实现类

```java
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckCaseValidator implements ConstraintValidator<CheckCase, String> {

    private CaseMode caseMode;

    @Override
    public void initialize(CheckCase constraintAnnotation) {
        this.caseMode = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null || "".equals(value.trim())) {
            return false;
        }

        switch (this.caseMode) {
            case LOWER:
                return value.equals(value.toLowerCase());
            case UPPER:
                return value.equals(value.toUpperCase());
            default:
                return false;
        }

    }

}
```
- `initialize()` 初始化时执行，可以用来获取注解中的属性
- `isValid()` 实现我们的校验逻辑

##### 备注
- 我们自定义的注解依然支持 @Validated group 分组

#### 手动使用 validator 校验
```java
import org.hibernate.validator.internal.engine.path.PathImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class ValidateUtil {

	/**
	 * 校验实体类
	 * 
	 * @param t
	 * @return
	 */
	public static <T> List<Map> validate(T t) {
		//定义返回错误List
		List<Map> errList = new ArrayList<>();
		Map<String, String> errorMap;
		
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<T>> errorSet = validator.validate(t);

		for (ConstraintViolation<T> c : errorSet) {
			errorMap = new HashMap<>();
			errorMap.put("field", c.getPropertyPath().toString()); //获取发生错误的字段
			errorMap.put("msg", c.getMessage()); //获取校验信息
			errList.add(errorMap);
		}

		return errList;
	}

	/**
	 * 使用 ValidList 校验List, 返回对应索引和错误消息
	 *
	 * @param t
	 * @param <T>
	 * @return
	 */
	public static <T> List<Map> validateList(T t) {
		//定义返回错误List
		List<Map> errList = new ArrayList<>();
		Map<String, Object> errorMap;

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<T>> errorSet = validator.validate(t);

		for (ConstraintViolation<T> c : errorSet) {
			errorMap = new HashMap<>();
			int index = ((PathImpl) c.getPropertyPath()).getLeafNode().getIndex();
			errorMap.put("index", index); // 当前索引
			errorMap.put("field", c.getPropertyPath().toString()); //获取发生错误的字段
			errorMap.put("msg", c.getMessage()); //获取校验信息
			errList.add(errorMap);
		}

		return errList;
	}

}

```

>#### 本节源码
https://gitee.com/yintianwen7/taven-springboot-learning/tree/master/springboot-validate
